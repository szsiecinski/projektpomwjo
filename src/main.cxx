#include <iostream>
#include <string>
#include <vector>
#include <cmath>

//ITK
#include <itkImage.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkImageSeriesReader.h>
#include <itkNumericSeriesFileNames.h>
//ITK - pomocnicze klasy
#include <itkGDCMImageIO.h>
#include <itkGDCMSeriesFileNames.h>

//filtry ITK
#include <itkCastImageFilter.h>
#include <itkRescaleIntensityImageFilter.h>
#include <itkCurvatureFlowImageFilter.h>
#include <itkNeighborhoodConnectedImageFilter.h>
#include <itkSliceBySliceImageFilter.h>
#include <itkAddImageFilter.h>

//iteratory
#include <itkLineIterator.h>
#include <itkImageRegionConstIteratorWithIndex.h>

//obiekty przestrzenne
#include <itkLineSpatialObject.h>
#include <itkLineSpatialObjectPoint.h>
#include <itkSpatialObjectToImageFilter.h>

//typedefy
typedef itk::Image<short, 2> Image2DType;
typedef itk::Image<short, 3> Image3DType;
typedef itk::Image<float, 3> FImage3DType;
typedef itk::ImageSeriesReader<Image3DType> SeriesReader;
typedef itk::ImageFileWriter<Image3DType> Writer3DType;

//struktura Punkt3D
template<typename T>
class Point3D {
public:
	T X;
	T Y;
	T Z;
	Point3D<T>(T x = 0, T y = 0, T z = 0) {
		this->X = x;
		this->Y = y;
		this->Z = z;
	}
	Point3D<T>(Point3D<T> &p) {
		this->X = p.X;
		this->Y = p.Y;
		this->Z = p.Z;
	}
	virtual ~Point3D<T>() {
	}
	double norm() {
		T x2 = this->X * this->X;
		T y2 = this->Y * this->Y;
		T z2 = this->Z * this->Z;
		float norm = sqrt(x2 + y2 + z2);
		return norm;
	}
	Point3D<T> operator+(Point3D<T> &p) {
		this->X += p.X;
		this->Y += p.Y;
		this->Z += p.Z;
		return *this;
	}
	Point3D<T> operator-(Point3D<T> &p) {
		this->X -= p.X;
		this->Y -= p.Y;
		this->Z -= p.Z;
		return *this;
	}
};

//funkcje
Image3DType::Pointer ReadSeries(std::string const &directory) {
	Image3DType::Pointer image;

	itk::GDCMImageIO::Pointer imgIO = itk::GDCMImageIO::New();

	//wczytywanie serii
	SeriesReader::Pointer seriesReader = SeriesReader::New();
	seriesReader->SetImageIO(imgIO);

	itk::GDCMSeriesFileNames::Pointer namesGenerator = itk::GDCMSeriesFileNames::New();
	namesGenerator->SetUseSeriesDetails(true);
	namesGenerator->SetDirectory(directory);

	itk::SerieUIDContainer series = namesGenerator->GetSeriesUIDs();

	seriesReader->SetFileNames(namesGenerator->GetFileNames(series[0]));
	image = seriesReader->GetOutput();
	seriesReader->Update();

	return image;
}

template<class ImageType>
void WriteImage(typename ImageType::Pointer image, const std::string &output) {
	auto writer = itk::ImageFileWriter<ImageType>::New();

	writer->SetFileName(output);
	writer->SetInput(image);
	writer->Update();
}

Image3DType::Pointer Smoothing(Image3DType::Pointer input) {
	typedef itk::CurvatureFlowImageFilter<Image3DType, FImage3DType> SmoothingFilter;
	typedef itk::CastImageFilter<FImage3DType, Image3DType> CastFilter;

	SmoothingFilter::Pointer smoothing = SmoothingFilter::New();
	CastFilter::Pointer cast = CastFilter::New();

	smoothing->SetInput(input);
	smoothing->SetNumberOfIterations(2);
	smoothing->SetTimeStep(0.125);

	cast->SetInput(smoothing->GetOutput());
	cast->Update();

	return cast->GetOutput();
}

Image3DType::Pointer Segmentation(Image3DType::Pointer input, int seedX, int seedY, int lowerThr, int upperThr) {

	typedef itk::NeighborhoodConnectedImageFilter<Image2DType, Image2DType> RegionGrowingFilter;
	typedef itk::SliceBySliceImageFilter<Image3DType, Image3DType> MainFilter;

	//rozrost obszaru
	RegionGrowingFilter::Pointer regionGrowing = RegionGrowingFilter::New();

	regionGrowing->SetLower(lowerThr);
	regionGrowing->SetUpper(upperThr);

	RegionGrowingFilter::IndexType seed;
	seed[0] = seedX;
	seed[1] = seedY;
	//seed[2] = slice;

	Image2DType::SizeType radius;
	radius[0] = 4;
	radius[1] = 4;
	regionGrowing->SetRadius(radius);

	regionGrowing->AddSeed(seed);

	//przetwarzanie warstwa po warstwie
	MainFilter::Pointer mainFilter = MainFilter::New();
	mainFilter->SetInput(input);
	mainFilter->SetFilter(regionGrowing);
	mainFilter->Update();

	return mainFilter->GetOutput();
}

Image3DType::Pointer DrawLine(Point3D<int> start, Point3D<int> end, Image3DType::Pointer image) {
	typedef itk::LineSpatialObject<3> Line;
	typedef itk::LineSpatialObjectPoint<3> Point;

	Line::Pointer line = Line::New();

	Line::PointListType pointList;

	Image3DType::IndexType startPoint, endPoint;
	startPoint[0] = start.X;
	startPoint[1] = start.Y;
	startPoint[2] = start.Z;

	endPoint[0] = end.X;
	endPoint[1] = end.Y;
	endPoint[2] = end.Z;

	itk::LineIterator<Image3DType> iterator(image, startPoint, endPoint);
	iterator.GoToBegin();

	Point p;
	double value = 0.5;

	while(!iterator.IsAtEnd()) {
		p.SetColor(value, value, value, value);
		Image3DType::IndexType position = iterator.GetIndex();

		p.SetPosition(position[0], position[1], position[2]);
		std::cout << "Biezacy piksel linii: " << iterator.GetIndex() << std::endl;
		pointList.push_back(p);
		++iterator;
	}

	line->SetPoints(pointList);
	line->SetId(1);

	typedef itk::SpatialObjectToImageFilter<Line, Image3DType> Object2Image;

	Object2Image::Pointer object2image = Object2Image::New();
	object2image->SetInput(line);
	object2image->SetSize(image->GetLargestPossibleRegion().GetSize());
	object2image->SetOutsideValue(0);
	object2image->SetInsideValue(0.6);

	object2image->Update();

	return object2image->GetOutput();
}

Image3DType::Pointer CombineLineAndObject(Image3DType::Pointer objectImage, Image3DType::Pointer lineImage) {
	lineImage->SetSpacing(objectImage->GetSpacing());
	lineImage->SetOrigin(objectImage->GetOrigin());
	lineImage->SetDirection(objectImage->GetDirection());

	typedef itk::AddImageFilter<Image3DType, Image3DType> ComposeImage;
	ComposeImage::Pointer composeImage = ComposeImage::New();
	composeImage->SetInput(0, objectImage);
	composeImage->SetInput(1,lineImage);

	composeImage->Update();
	return composeImage->GetOutput();
}

int main(int argc, char **argv) {

	//wczytywanie serii
	std::string sciezka, wynik;
	int seedX, seedY, lowerThr, upperThr;

	switch(argc) {
	case 7:
		sciezka = argv[1];
		wynik = argv[2];
		seedX = atoi(argv[3]);
		seedY = atoi(argv[4]);
		lowerThr = atoi(argv[5]);
		upperThr = atoi(argv[6]);
		break;
	case 6:
		sciezka = argv[1];
		wynik = argv[2];
		seedX = atoi(argv[3]);
		seedY = atoi(argv[4]);
		lowerThr = 490;
		upperThr = atoi(argv[5]);
		break;
	case 5:
		sciezka = argv[1];
		wynik = argv[2];
		seedX = atoi(argv[3]);
		seedY = atoi(argv[4]);
		lowerThr = 490;
		upperThr = 1400;
		break;
	case 4:
		sciezka = argv[1];
		wynik = argv[2];
		seedX = 135;
		seedY = 175;
		lowerThr = 490;
		upperThr = atoi(argv[3]);
		break;
	case 3:
		sciezka = argv[1];
		wynik = argv[2];
		seedX = 135;
		seedY = 175;
		lowerThr = 490;
		upperThr = 1400;
		break;
	case 2:
		sciezka = argv[1];
		wynik = "../output.vtk";
		seedX = 135;
		seedY = 175;
		lowerThr = 490;
		upperThr = 1400;
		break;
	default:
		sciezka = "../dane/sk-s01";
		wynik = "../output.vtk";
		seedX = 135;
		seedY = 175;
		lowerThr = 490;
		upperThr = 1400;
		break;
	}

	Image3DType::Pointer image = ReadSeries(sciezka);
	auto spacing = image->GetSpacing();
	auto filteredImage = Segmentation(image, seedX, seedY, lowerThr, upperThr);
	filteredImage->SetSpacing(spacing);

	typedef itk::ImageRegionIteratorWithIndex<Image3DType> ImageIterator;
	ImageIterator iterator(filteredImage, filteredImage->GetRequestedRegion());

	typedef struct {
		int minX =0;
		int maxX =0;
		int minY =0;
		int maxY =0;
		int minZ =0;
		int maxZ =0;
	} MinMaxXYZ;

	MinMaxXYZ minmax;

	for(iterator.GoToBegin(); !iterator.IsAtEnd(); ++iterator) {
		Image3DType::IndexType index = iterator.GetIndex();
		Image3DType::PixelType pixel = filteredImage->GetPixel(index);

		if (pixel > 0) {
			if (minmax.maxX < index[0])
				minmax.maxX = index[0];

			if (minmax.maxY < index[1])
				minmax.maxY = index[1];

			if (minmax.maxZ < index[2])
				minmax.maxZ = index[2];

			if (minmax.minX > index[0])
				minmax.minX = index[0];

			if (minmax.minY > index[1])
				minmax.minY = index[1];

			if (minmax.minZ > index[2])
				minmax.minZ = index[2];
		}

	}

	std::cout << "obszar" << std::endl;
	std::cout << "minX = " << minmax.minX << std::endl;
	std::cout << "minY = " << minmax.minY << std::endl;
	std::cout << "minZ = " << minmax.minZ << std::endl;

	std::cout << "maxX = " << minmax.maxX << std::endl;
	std::cout << "maxY = " << minmax.maxY << std::endl;
	std::cout << "maxZ = " << minmax.maxZ << std::endl;

	Point3D<int> start, end;
	start.X = (minmax.maxX - minmax.minX)/2;
	start.Y = minmax.minY;
	start.Z = (minmax.maxZ - minmax.minZ)/2;

	end.X = (minmax.maxX - minmax.minX)/2;
	end.Y = minmax.maxY;
	end.Z = (minmax.maxZ - minmax.minZ)/2;

	auto bone_axis = DrawLine(start, end, filteredImage);
	auto compositeImage = CombineLineAndObject(filteredImage, bone_axis);

	typedef itk::RescaleIntensityImageFilter<Image3DType> RescaleIntensity;
	RescaleIntensity::Pointer rescaleIntensity = RescaleIntensity::New();
	rescaleIntensity->SetInput(compositeImage);

	rescaleIntensity->Update();
	compositeImage = rescaleIntensity->GetOutput();

	try {
		WriteImage<Image3DType>(compositeImage, wynik);
		WriteImage<Image3DType>(image, "../source.vtk");

		std::cout << "OK! Wcisnij ENTER." << std::endl;
		std::cin.get();

	}
	catch(itk::ExceptionObject &ex) {
		std::cerr << ex.what() << std::endl;
		return -1;
	}
	catch(std::exception &ex) {
		std::cerr << ex.what() << std::endl;
		return -1;
	}
	catch(int ex) {
		std::cerr << "Wyjatek " << ex << std::endl;
		return -1;
	}
	catch(...) {
		std::cerr << "Inny wyjatek" << std::endl;
		return -1;
	}

	return 0;
}
